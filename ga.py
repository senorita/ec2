import argparse
import random
from typing import Union, Dict, Optional, List, Tuple

import gym
import numpy as np
import torch
import tqdm
from torch import nn

PolicyConfig = Dict[str, int]
POLICY_CONFIGS: Dict[str, PolicyConfig] = {
    'CartPole-v1': {'state_len': 4, 'action_len': 2, 'hidden_size_1': 20, 'hidden_size_2': 12},
    'LunarLander-v2': {'state_len': 8, 'action_len': 4, 'hidden_size_1': 200, 'hidden_size_2': 120},
}
ENV_CONFIGS: Dict[str, Dict[str, int]] = {
    'CartPole-v1': {'episode_steps': 200},
    'LunarLander-v2': {'episode_steps': 300},
}


class PolicyWeightsCandidate:
    def __init__(self, weights: torch.Tensor):
        self.weights: torch.Tensor = weights
        self._fitness: Optional[float] = None

    @property
    def fitness(self) -> float:
        assert self._fitness is not None
        return self._fitness

    @fitness.setter
    def fitness(self, f: float):
        self._fitness = f

    @staticmethod
    def random(config: PolicyConfig):
        return Policy(**config).get_weights()

    def clone(self):
        return PolicyWeightsCandidate(self.weights.clone())

    def __repr__(self):
        return f"Candidate (fitness: {self.fitness})"


class Policy(nn.Module):
    def __init__(self, state_len: int, action_len: int, hidden_size_1: int, hidden_size_2: int):
        super().__init__()
        self.ff1 = nn.Linear(state_len, hidden_size_1)
        self.d1 = nn.Dropout(0.2)
        self.ff2 = nn.Linear(hidden_size_1, hidden_size_2)
        self.d2 = nn.Dropout(0.2)
        self.ff3 = nn.Linear(hidden_size_2, action_len)

    def forward(self, state: Union[np.ndarray, torch.Tensor]):
        if type(state) == np.ndarray:
            state = torch.Tensor(state)
        assert type(state) == torch.Tensor

        out = state
        out = self.ff1(out)
        # out = self.d1(out)
        out = torch.relu(out)
        out = self.ff2(out)
        # out = self.d2(out)
        out = torch.relu(out)
        out = self.ff3(out)
        out = torch.softmax(out, 0)

        return out.argmax().item()

    def set_weights(self, candidate: PolicyWeightsCandidate):
        weights = candidate.weights
        params = [
            self.ff1.weight, self.ff1.bias,
            self.ff2.weight, self.ff2.bias,
            self.ff3.weight, self.ff3.bias
        ]
        weight_idx = 0
        for param in params:
            param.data = weights[weight_idx: weight_idx + param.shape.numel()].reshape(param.shape)
            weight_idx += param.shape.numel()

    def get_weights(self) -> PolicyWeightsCandidate:
        return PolicyWeightsCandidate(torch.cat([
            self.ff1.weight.data.flatten(), self.ff1.bias.data.flatten(),
            self.ff2.weight.data.flatten(), self.ff2.bias.data.flatten(),
            self.ff3.weight.data.flatten(), self.ff3.bias.data.flatten(),
        ]))


class WeightMutator:
    def __init__(self):
        self.mutation_prob = 0.5

    def mutate(self, c: PolicyWeightsCandidate) -> PolicyWeightsCandidate:
        res = c.clone()

        # Change random weight.
        if torch.rand(1) < self.mutation_prob:
            scale = 10
            n = torch.randint(0, res.weights.shape.numel(), [1])
            # new_val = torch.normal(res.weights.mean(), res.weights.std())
            new_val = torch.normal(res.weights.mean(), res.weights.std()) + torch.randint(-scale, scale, (1,))
            res.weights[n] = new_val

        return res


class WeightCrossover:
    def __init__(self):
        self.crossover_prob = 0.1

    def crossover(self, c1: PolicyWeightsCandidate, c2: PolicyWeightsCandidate) -> Tuple[PolicyWeightsCandidate, PolicyWeightsCandidate]:
        c1, c2 = c1.clone(), c2.clone()

        # Swap random weight.
        if torch.rand(1) < self.crossover_prob:
            n = torch.randint(0, c1.weights.shape.numel(), [1])
            c1.weights[n], c2.weights[n] = c2.weights[n], c1.weights[n]

        return c1, c2


class PolicyTrainer:
    def __init__(self, cartpole: bool = True, pop_size: int = 10):
        self.population_size = pop_size
        self.population: List[PolicyWeightsCandidate] = []
        self.mutator = WeightMutator()
        self.crossover = WeightCrossover()

        self.best_candidate = None
        self.curr_best_candidate = None

        env_name = 'CartPole-v1' if cartpole else 'LunarLander-v2'
        self.env = gym.make(env_name)
        self.env_config = ENV_CONFIGS[env_name]
        self.policy_config = POLICY_CONFIGS[env_name]

        seed = 42
        self.env.seed(seed)
        torch.manual_seed(seed)
        np.random.seed(seed)
        random.seed(seed)

    def init(self):
        self.population = []
        for _ in range(self.population_size):
            c = PolicyWeightsCandidate.random(self.policy_config)
            self.evaluate(c)
            self.population.append(c)
        self.best_candidate = max(self.population, key=lambda c: c.fitness)

    def select_parents(self) -> Tuple[PolicyWeightsCandidate, PolicyWeightsCandidate]:
        # Hack alert.
        distr = torch.distributions.Categorical(logits=torch.tensor([c.fitness for c in self.population]))
        return tuple(self.population[i] for i in torch.multinomial(distr.probs, 2))

    def generation(self):
        new_pop = []
        for _ in range(0, self.population_size, 2):
            parent1, parent2 = self.select_parents()

            c1, c2 = self.mutator.mutate(parent1), self.mutator.mutate(parent2)

            c1, c2 = self.crossover.crossover(c1, c2)

            self.evaluate(c1, final=False)
            self.evaluate(c2, final=False)

            if min(c1.fitness, c2.fitness) > max(parent1.fitness, parent2.fitness):
                new_pop.append(c1)
                new_pop.append(c2)
            else:
                new_pop.append(parent1)
                new_pop.append(parent2)
        self.population = new_pop

        self.curr_best_candidate = max(self.population, key=lambda c: c.fitness)
        self.best_candidate = max([self.curr_best_candidate, self.best_candidate], key=lambda c: c.fitness)

    def mutation(self):
        mutated_pop = []
        for p in self.population:
            mutated_pop.append(self.mutator.mutate(p))
        return mutated_pop

    def fitness(self, c: PolicyWeightsCandidate, final=True):
        policy = Policy(**self.policy_config)
        policy.set_weights(c)

        if final:
            policy.eval()
        else:
            policy.train()

        s = self.env.reset()
        cumm_reward = 0
        for _ in range(self.env_config['episode_steps']):
            with torch.no_grad():
                a = policy(s)
            s, r, done, _ = self.env.step(a)
            cumm_reward += r
            # if done:
            #     break
        return cumm_reward

    def evaluate(self, c: PolicyWeightsCandidate, final=True):
        # assert c._fitness is None, "A candidate has already been evaluated"
        c.fitness = self.fitness(c, final=final)

    def best_fitness(self, curr=False):
        fitnesses = []
        c = self.best_candidate if not curr else self.curr_best_candidate
        for i in range(5):
            fitnesses.append(self.fitness(c))
        return np.mean(fitnesses)


def main(args: argparse.Namespace):
    trainer = PolicyTrainer(args.env == 'cart_pole', args.pop_size)
    trainer.init()
    pbar = tqdm.tqdm(range(args.gens))
    for _ in pbar:
        trainer.generation()
        pbar.set_description(f'Best fitness: {trainer.best_fitness()}, curr raw fitness: {trainer.best_fitness(True)}')
    print("Final fitness:", trainer.best_fitness())


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--env', default='cart_pole', choices=['cart_pole', 'lunar_lander'])
    parser.add_argument('--pop_size', type=int, default=50)
    parser.add_argument('--gens', type=int, default=20)
    main(parser.parse_args())
